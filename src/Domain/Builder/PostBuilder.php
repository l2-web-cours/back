<?php

namespace App\Domain\Builder;

use App\Domain\Builder\Interfaces\PostBuilderInterface;
use App\Domain\DTO\PostDTO;
use App\Domain\Model\Interfaces\PostInterface;
use App\Domain\Model\Interfaces\UserInterface;
use App\Domain\Model\Post;

class PostBuilder implements PostBuilderInterface
{
    /**
     * @var PostInterface
     */
    private $post;

    public function build(PostDTO $postDTO, UserInterface $user): PostBuilderInterface
    {
        $this->post = new Post(
            $postDTO->title,
            $postDTO->content,
            $user
        );

        return $this;
    }

    public function getPost(): PostInterface
    {
        return $this->post;
    }
}
