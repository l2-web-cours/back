<?php

namespace App\Domain\Builder\Interfaces;

use App\Domain\Builder\PostBuilder;
use App\Domain\DTO\PostDTO;
use App\Domain\Model\Interfaces\PostInterface;
use App\Domain\Model\Interfaces\UserInterface;

interface PostBuilderInterface
{
    /**
     * @param PostDTO       $postDTO
     * @param UserInterface $user
     *
     * @return PostBuilder
     */
    public function build(PostDTO $postDTO, UserInterface $user): self;

    /**
     * @return PostInterface
     */
    public function getPost(): PostInterface;
}