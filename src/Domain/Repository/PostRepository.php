<?php

namespace App\Domain\Repository;

use App\Domain\Model\Interfaces\PostInterface;
use App\Domain\Model\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @param PostInterface $post
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(PostInterface $post)
    {
        $this->_em->persist($post);
        $this->_em->flush();
    }

    /**
     * @param int $id
     *
     * @return PostInterface|null
     * @throws NonUniqueResultException
     */
    public function loadPostById(int $id): ?PostInterface
    {
        return $this->createQueryBuilder('p')
                    ->where('p.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    /**
     * @return PostInterface[]
     */
    public function loadPosts(): array
    {
        return $this->createQueryBuilder('p')
                    ->getQuery()
                    ->execute();
    }

    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(int $id)
    {
        $this->_em->remove($this->loadPostById($id));
        $this->_em->flush();
    }
}
