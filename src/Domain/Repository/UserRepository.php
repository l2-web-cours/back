<?php

namespace App\Domain\Repository;

use App\Domain\Model\Interfaces\UserInterface;
use App\Domain\Model\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function loadUsers(): ?array
    {
        return $this->createQueryBuilder('u')
                    ->getQuery()
                    ->execute();
    }

    /**
     * @param int $id
     *
     * @return UserInterface|null
     * @throws NonUniqueResultException
     */
    public function LoadUserById(int $id): ?UserInterface
    {
        return $this->createQueryBuilder('u')
                    ->where('u.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
}
