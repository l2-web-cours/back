<?php

namespace App\Domain\Repository;

use App\Domain\Model\Pictures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class PictureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pictures::class);
    }

    /**
     * @param int $paginationId
     *
     * @return Pictures[]|null
     */
    public function loadWithPagination(int $paginationId): array
    {
        return $this->createQueryBuilder('picture')
            ->setFirstResult((5 * $paginationId - 5))
            ->setMaxResults(5)
            ->getQuery()
            ->getArrayResult();
    }
}
