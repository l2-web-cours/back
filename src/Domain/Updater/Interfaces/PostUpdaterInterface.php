<?php

namespace App\Domain\Updater\Interfaces;

use App\Application\Interfaces\OutInterface;

interface PostUpdaterInterface
{
    public function update(int $id, string $credentials): OutInterface;
}