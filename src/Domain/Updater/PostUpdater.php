<?php

namespace App\Domain\Updater;

use App\Application\Interfaces\OutInterface;
use App\Domain\DTO\PostDTO;
use App\Domain\Output\PostOutput;
use App\Domain\Repository\PostRepository;
use App\Domain\Repository\UserRepository;
use App\Domain\Updater\Interfaces\PostUpdaterInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use http\Client\Curl\User;
use Symfony\Component\Serializer\SerializerInterface;

class PostUpdater implements PostUpdaterInterface
{
    /**
     * @var PostRepository
     */
    private $postRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * PostUpdater constructor.
     *
     * @param PostRepository      $postRepository
     * @param UserRepository      $userRepository
     * @param SerializerInterface $serializer
     */
    public function __construct(
        PostRepository $postRepository,
        UserRepository $userRepository,
        SerializerInterface $serializer
    ) {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
        $this->serializer = $serializer;
    }

    /**
     * @param int    $id
     * @param string $credentials
     *
     * @return OutInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(int $id, string $credentials): OutInterface
    {
        /** @var PostDTO $postDTO */
        $postDTO = $this->serializer->deserialize($credentials, PostDTO::class, 'json');
        $user = null;
        if ($postDTO->userId) {
            $user = $this->userRepository->LoadUserById($postDTO->userId);
        }

        $post = $this->postRepository->loadPostById($id);
        $post->updatePost($postDTO, $user);
        $this->postRepository->save($post);

        return new PostOutput($post);
    }
}
