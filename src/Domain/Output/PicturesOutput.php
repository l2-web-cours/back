<?php

namespace App\Domain\Output;

use App\Application\Interfaces\ItemInterface;
use App\Domain\Model\Pictures;
use App\Domain\Output\Interfaces\PicturesOutputInterface;

class PicturesOutput implements PicturesOutputInterface
{
    /**
     * @var Pictures[]
     */
    private $pictures;

    /**
     * PicturesOutput constructor.
     *
     * @param $pictures
     */
    public function __construct($pictures)
    {
        $this->pictures = $pictures;
    }

    /**
     * @return ItemInterface[]
     */
    public function getItems(): array
    {
        return $this->pictures;
    }
}
