<?php

namespace App\Domain\Output;

use App\Application\Interfaces\ItemInterface;
use App\Domain\Model\Interfaces\PostInterface;
use App\Domain\Output\Interfaces\PostsOutputInterface;

class PostsOutput implements PostsOutputInterface
{
    /**
     * @var PostInterface[]
     */
    private $posts;

    /**
     * PostOutput constructor.
     *
     * @param PostInterface[] $posts
     */
    public function __construct(array $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return ItemInterface[]
     */
    public function getItems(): array
    {
        return $this->posts;
    }
}
