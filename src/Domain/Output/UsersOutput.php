<?php

namespace App\Domain\Output;

use App\Application\Interfaces\ItemInterface;
use App\Domain\Model\User;
use App\Domain\Output\Interfaces\UsersOutputInterface;

class UsersOutput implements UsersOutputInterface
{
    /**
     * @var User[]
     */
    private $users;

    /**
     * UsersOutput constructor.
     *
     * @param User[] $users
     */
    public function __construct(array $users)
    {
        $this->users = $users;
    }

    /**
     * @return ItemInterface[]
     */
    public function getItems(): array
    {
        return $this->users;
    }

    /**
     * @param User $user
     */
    public function addUser(User $user): void
    {
        $this->users[] = $user;
    }
}
