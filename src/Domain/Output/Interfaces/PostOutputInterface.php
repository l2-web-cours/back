<?php

namespace App\Domain\Output\Interfaces;

use App\Application\Interfaces\OutputInterface;

interface PostOutputInterface extends OutputInterface
{
}