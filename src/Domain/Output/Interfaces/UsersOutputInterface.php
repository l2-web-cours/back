<?php

namespace App\Domain\Output\Interfaces;

use App\Application\Interfaces\OutputsInterface;
use App\Domain\Model\User;

interface UsersOutputInterface extends OutputsInterface
{
    /**
     * @param User $user
     */
    public function addUser(User $user): void;
}