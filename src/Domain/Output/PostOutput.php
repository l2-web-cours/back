<?php

namespace App\Domain\Output;

use App\Application\Interfaces\ItemInterface;
use App\Domain\Model\Interfaces\PostInterface;
use App\Domain\Output\Interfaces\PostOutputInterface;

class PostOutput implements PostOutputInterface
{
    /**
     * @var PostInterface|null
     */
    private $post;

    /**
     * PostOutput constructor.
     *
     * @param PostInterface $post
     */
    public function __construct(?PostInterface $post = null)
    {
        $this->post = $post;
    }

    /**
     * @return PostInterface|null
     */
    public function getItem(): ?ItemInterface
    {
        return $this->post;
    }
}
