<?php

namespace App\Domain\DTO;

class PostDTO
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $content;
    /**
     * @var int
     */
    public $userId;

    /**
     * PostDTO constructor.
     *
     * @param string|null $title
     * @param string|null $content
     * @param int|null    $userId
     */
    public function __construct(
        ?string $title = null,
        ?string $content = null,
        ?int $userId = null
    ) {
        $this->title = $title;
        $this->content = $content;
        $this->userId = $userId;
    }
}
