<?php

namespace App\Domain\Loader;

use App\Domain\Output\Interfaces\UsersOutputInterface;
use App\Domain\Output\UsersOutput;
use App\Domain\Repository\UserRepository;

class UsersLoader
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UsersLoader constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function load(): UsersOutputInterface
    {
        return new UsersOutput($this->userRepository->loadUsers());
    }
}
