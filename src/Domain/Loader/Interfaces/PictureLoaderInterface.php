<?php

namespace App\Domain\Loader\Interfaces;

use App\Application\Interfaces\OutputsInterface;

interface PictureLoaderInterface
{
    /**
     * Charge les noms des images. Renvoie null si on a dépassé la navigation.
     *
     * @param int $paginationId
     *
     * @return OutputsInterface|null
     */
    public function load(int $paginationId): ?OutputsInterface;
}