<?php

namespace App\Domain\Loader\Interfaces;

use App\Application\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\PostOutputInterface;
use App\Domain\Output\Interfaces\PostsOutputInterface;
use Doctrine\ORM\NonUniqueResultException;

interface PostLoaderInterface
{
    /**
     * @param int|null $id
     *
     * @return PostOutputInterface|PostsOutputInterface
     * @throws NonUniqueResultException
     */
    public function load(?int $id = null): OutInterface;
}