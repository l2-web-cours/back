<?php

namespace App\Domain\Loader;

use App\Application\Interfaces\OutInterface;
use App\Domain\Loader\Interfaces\PostLoaderInterface;
use App\Domain\Output\PostOutput;
use App\Domain\Output\PostsOutput;
use App\Domain\Repository\PostRepository;

class PostLoader implements PostLoaderInterface
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * PostLoader constructor.
     *
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function load(?int $id = null): OutInterface
    {
        return $id
            ? new PostOutput($this->postRepository->loadPostById($id))
            : new PostsOutput($this->postRepository->loadPosts());
    }
}
