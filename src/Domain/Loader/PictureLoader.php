<?php

namespace App\Domain\Loader;

use App\Application\Interfaces\OutputsInterface;
use App\Domain\Loader\Interfaces\PictureLoaderInterface;
use App\Domain\Output\PicturesOutput;
use App\Domain\Repository\PictureRepository;
use Exception;

class PictureLoader implements PictureLoaderInterface
{
    /**
     * @var PictureRepository
     */
    private $pictureRepository;

    /**
     * PictureLoader constructor.
     *
     * @param PictureRepository $pictureRepository
     */
    public function __construct(PictureRepository $pictureRepository)
    {
        $this->pictureRepository = $pictureRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function load(int $paginationId): ?OutputsInterface
    {
        return empty($this->pictureRepository->loadWithPagination($paginationId))
            ? null
            : new PicturesOutput($this->pictureRepository->loadWithPagination($paginationId));
    }
}
