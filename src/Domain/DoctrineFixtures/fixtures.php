<?php

namespace App\Domain\DoctrineFixtures;

use App\Domain\Model\Pictures;
use App\Domain\Model\Post;
use App\Domain\Model\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class fixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $JohnDoe = new User('John', 'Doe', 'JohnDoe', 'john@doe.fr');
        $manager->persist($JohnDoe);
        $JaneDoe = new User('Jane', 'Doe', 'JaneDoe', 'jane@doe.fr');
        $manager->persist($JaneDoe);

        $manager->flush();

        $post1 = new Post(
            'Articlleeeeu 1',
            'Zombies reversus ab inferno, nam malum cerebro.',
            $JohnDoe
        );
        $manager->persist($post1);
        $post2 = new Post(
            'Der zweiter Artikel',
            'Lorem Elsass ipsum placerat mamsell ac dui ornare gravida libero. tchao bissame gewurztraminer  Oberschaeffolsheim Racing. Yo dû. Salut bisamme Oberschaeffolsheim Strasbourg non kougelhopf schnaps amet, morbi auctor, kuglopf kartoffelsalad eleifend purus Salu bissame Gal ! knepfle elementum Morbi mollis vielmols, météor quam. sed wie consectetur dolor picon bière Miss Dahlias so elit hop Christkindelsmärik et Hans Pfourtz ! dignissim salu semper leo lotto-owe mänele leo eget libero, Wurschtsalad porta barapli geht\'s Chulien Chulia Roberstau Huguette DNA, adipiscing suspendisse sit Coopé de Truchtersheim sit ullamcorper réchime hopla risus, sed tristique s\'guelt merci vielmols hoplageiss knack jetz gehts los ac nüdle hopla wurscht blottkopf, bredele turpis, amet habitant pellentesque Richard Schirmeck Carola Verdammi varius Spätzle yeuh. rossbolla Pellentesque id, hopla sagittis schneck rucksack geïz condimentum id leverwurscht messti de Bischheim libero, Kabinetpapier tellus ante flammekueche turpis schpeck lacus hopla quam, Mauris in, vulputate ornare chambon gal non commodo senectus Gal. und tellus aliquam bissame nullam sit munster rhoncus ftomi! Heineken ch\'ai baeckeoffe amet',
            $JaneDoe
        );
        $manager->persist($post2);

        $picture1 = new Pictures('SJBCojkxQm');
        $manager->persist($picture1);
        $picture2 = new Pictures('jxODjeIUZl');
        $manager->persist($picture2);
        $picture3 = new Pictures('FzqUXULFbq');
        $manager->persist($picture3);
        $picture4 = new Pictures('fHnHLxkdCp');
        $manager->persist($picture4);
        $picture5 = new Pictures('cILCsSvuXF');
        $manager->persist($picture5);
        $picture6 = new Pictures('rxcYtNHBWj');
        $manager->persist($picture6);
        $picture7 = new Pictures('JHrNKFvinb');
        $manager->persist($picture7);
        $picture8 = new Pictures('tBuAZhOQVU');
        $manager->persist($picture8);
        $picture9 = new Pictures('mEtFopdLaB');
        $manager->persist($picture9);
        $picture10 = new Pictures('rGSpQLJANx');
        $manager->persist($picture10);
        $picture11 = new Pictures('mukBTPSrla');
        $manager->persist($picture11);
        $picture12 = new Pictures('lTYBgYugtC');
        $manager->persist($picture12);
        $picture13 = new Pictures('LRsXuPzfoF');
        $manager->persist($picture13);
        $picture14 = new Pictures('CTJYNTZPsK');
        $manager->persist($picture14);
        $picture15 = new Pictures('sDmqgvShuy');
        $manager->persist($picture15);
        $picture16 = new Pictures('zrXImDLUkP');
        $manager->persist($picture16);
        $picture17 = new Pictures('oFcSxbzVeZ');
        $manager->persist($picture17);
        $picture18 = new Pictures('HjTRcrHKip');
        $manager->persist($picture18);

        $manager->flush();
    }
}
