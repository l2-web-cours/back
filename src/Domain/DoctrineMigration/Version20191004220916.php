<?php

declare(strict_types=1);

namespace App\Domain\DoctrineMigration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191004220916 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE ajax_picture (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(10) NOT NULL)');
        $this->addSql('DROP INDEX IDX_28DCD8F3A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__server_posts AS SELECT id, user_id, title, content FROM server_posts');
        $this->addSql('DROP TABLE server_posts');
        $this->addSql('CREATE TABLE server_posts (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, title VARCHAR(200) NOT NULL COLLATE BINARY, content CLOB NOT NULL COLLATE BINARY, CONSTRAINT FK_28DCD8F3A76ED395 FOREIGN KEY (user_id) REFERENCES server_users (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO server_posts (id, user_id, title, content) SELECT id, user_id, title, content FROM __temp__server_posts');
        $this->addSql('DROP TABLE __temp__server_posts');
        $this->addSql('CREATE INDEX IDX_28DCD8F3A76ED395 ON server_posts (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE ajax_picture');
        $this->addSql('DROP INDEX IDX_28DCD8F3A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__server_posts AS SELECT id, user_id, title, content FROM server_posts');
        $this->addSql('DROP TABLE server_posts');
        $this->addSql('CREATE TABLE server_posts (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, title VARCHAR(200) NOT NULL, content CLOB NOT NULL)');
        $this->addSql('INSERT INTO server_posts (id, user_id, title, content) SELECT id, user_id, title, content FROM __temp__server_posts');
        $this->addSql('DROP TABLE __temp__server_posts');
        $this->addSql('CREATE INDEX IDX_28DCD8F3A76ED395 ON server_posts (user_id)');
    }
}
