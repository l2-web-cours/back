<?php

namespace App\Domain\Saver;

use App\Application\Interfaces\OutputInterface;
use App\Domain\Builder\Interfaces\PostBuilderInterface;
use App\Domain\DTO\PostDTO;
use App\Domain\Output\PostOutput;
use App\Domain\Repository\PostRepository;
use App\Domain\Repository\UserRepository;
use App\Domain\Saver\Interfaces\PostSaverInterface;
use Symfony\Component\Serializer\SerializerInterface;

class PostSaver implements PostSaverInterface
{
    /**
     * @var PostRepository
     */
    private $postRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var PostBuilderInterface
     */
    private $postBuilder;

    /**
     * PostSaver constructor.
     *
     * @param PostRepository       $postRepository
     * @param UserRepository       $userRepository
     * @param SerializerInterface  $serializer
     * @param PostBuilderInterface $postBuilder
     */
    public function __construct(
        PostRepository $postRepository,
        UserRepository $userRepository,
        SerializerInterface $serializer,
        PostBuilderInterface $postBuilder
    ) {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
        $this->serializer = $serializer;
        $this->postBuilder = $postBuilder;
    }

    /**
     * {@inheritDoc}
     */
    public function save(string $credentials): OutputInterface
    {
        /** @var PostDTO $postDTO */
        $postDTO = $this->serializer->deserialize($credentials, PostDTO::class, 'json');

        $post = $this->postBuilder->build($postDTO, $this->userRepository->LoadUserById($postDTO->userId))->getPost();

        $this->postRepository->save($post);

        return new PostOutput($post);
    }
}
