<?php

namespace App\Domain\Saver\Interfaces;

use App\Application\Interfaces\OutputInterface;
use App\Domain\Output\PostOutput;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

interface PostSaverInterface
{
    /**
     * @param string $credentials
     *
     * @return PostOutput
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(string $credentials): OutputInterface;
}