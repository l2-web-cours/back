<?php

namespace App\Domain\Deleter;

use App\Domain\Deleter\Interfaces\PostDeleterInterface;
use App\Domain\Repository\PostRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class PostDeleter implements PostDeleterInterface
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * PostDeleter constructor.
     *
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(int $id): bool
    {
        try {
            $this->postRepository->delete($id);
        } catch (NonUniqueResultException | ORMException | OptimisticLockException $exception) {
            return false;
        }

        return true;
    }
}
