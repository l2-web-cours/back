<?php

namespace App\Domain\Deleter\Interfaces;

interface PostDeleterInterface
{
    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool;
}