<?php

namespace App\Domain\Model\Interfaces;


use App\Application\Interfaces\ItemInterface;

/**
 * Class Post
 *
 * @package App\Domain\Model
 * @ORM\Table(name="ajax_picture")
 * @ORM\Entity(repositoryClass=App\Domain\Repository\PictureRepository")
 */
interface PicturesInterface extends ItemInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getName(): string;
}