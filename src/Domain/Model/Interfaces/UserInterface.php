<?php

namespace App\Domain\Model\Interfaces;


use App\Application\Interfaces\ItemInterface;

/**
 * Class User
 */
interface UserInterface extends ItemInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getFirstName(): string;

    /**
     * @return string
     */
    public function getLastName(): string;

    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @return string
     */
    public function getMail(): string;
}