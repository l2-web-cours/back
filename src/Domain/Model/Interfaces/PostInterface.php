<?php

namespace App\Domain\Model\Interfaces;


use App\Application\Interfaces\ItemInterface;
use App\Domain\DTO\PostDTO;

/**
 * Class Post
 */
interface PostInterface extends ItemInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return string
     */
    public function getContent(): string;

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface;

    /**
     * @param PostDTO       $postDTO
     * @param UserInterface $user
     */
    public function updatePost(PostDTO $postDTO, ?UserInterface $user = null): void;
}