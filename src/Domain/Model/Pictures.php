<?php

namespace App\Domain\Model;


use App\Domain\Model\Interfaces\PicturesInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 *
 * @package App\Domain\Model
 * @ORM\Table(name="ajax_picture")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\PictureRepository")
 */
class Pictures implements PicturesInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=10)
     */
    private $name;

    /**
     * Pictures constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
