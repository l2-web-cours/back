<?php

namespace App\Domain\Model;

use App\Domain\Model\Interfaces\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 *
 * @package App\Domain\Model
 * @ORM\Table(name="server_users")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $mail;

    /**
     * User constructor.
     *
     * @param string $firstName
     * @param string $lastName
     * @param string $username
     * @param string $mail
     */
    public function __construct(string $firstName, string $lastName, string $username, string $mail)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->username = $username;
        $this->mail = $mail;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }
}
