<?php

namespace App\Domain\Model;


use App\Domain\DTO\PostDTO;
use App\Domain\Model\Interfaces\PostInterface;
use App\Domain\Model\Interfaces\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 *
 * @package App\Domain\Model
 * @ORM\Table(name="server_posts")
 * @ORM\Entity()
 */
class Post implements PostInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=200)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var UserInterface
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\User", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, name="user_id")
     */
    private $user;

    /**
     * Post constructor.
     *
     * @param string        $title
     * @param string        $content
     * @param UserInterface $user
     */
    public function __construct(
        string $title,
        string $content,
        UserInterface $user
    ) {
        $this->title = $title;
        $this->content = $content;
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @param PostDTO       $postDTO
     * @param UserInterface $user
     */
    public function updatePost(PostDTO $postDTO, ?UserInterface $user = null): void
    {
        if ($user) {
            $this->user = $user;
        }

        if ($postDTO->content) {
            $this->content = $postDTO->content;
        }

        if ($postDTO->title) {
            $this->title = $postDTO->title;
        }
    }
}
