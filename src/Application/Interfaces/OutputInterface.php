<?php

namespace App\Application\Interfaces;

interface OutputInterface extends OutInterface
{
    /**
     * @return ItemInterface|null
     */
    public function getItem(): ?ItemInterface;
}
