<?php

namespace App\Application\Interfaces;

interface OutputsInterface extends OutInterface
{
    /**
     * @return ItemInterface[]
     */
    public function getItems(): array;
}
