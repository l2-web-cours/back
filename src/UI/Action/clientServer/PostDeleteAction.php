<?php

namespace App\UI\Action\clientServer;

use App\Domain\Deleter\Interfaces\PostDeleterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostDeleteAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var PostDeleterInterface
     */
    private $postDeleter;

    /**
     * PostDeleteAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param PostDeleterInterface  $postDeleter
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        PostDeleterInterface $postDeleter
    ) {
        $this->apiResponder = $apiResponder;
        $this->postDeleter = $postDeleter;
    }

    /**
     * @Route(
     *     path="/articles/{id}",
     *     requirements={"\d+"},
     *     methods={"DELETE"}
     * )
     * @param int $id
     *
     * @return Response
     */
    public function delete(int $id): Response
    {
        return $this->postDeleter->delete($id)
            ? $this->apiResponder->response(null, 204)
            : $this->apiResponder->response(null, 404);
    }
}
