<?php

namespace App\UI\Action\clientServer;

use App\Domain\Updater\Interfaces\PostUpdaterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostUpdateAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var PostUpdaterInterface
     */
    private $postUpdater;

    /**
     * PostUpdateAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param PostUpdaterInterface  $postUpdater
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        PostUpdaterInterface $postUpdater
    ) {
        $this->apiResponder = $apiResponder;
        $this->postUpdater = $postUpdater;
    }

    /**
     * @Route(
     *     path="/articles/{id}",
     *     requirements={"\d+"},
     *     name="postUpdate",
     *     methods={"PUT", "PATCH"}
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function update(Request $request, int $id): Response
    {
        return $this->apiResponder->response(
            $postUpdated = $this->postUpdater->update(
                $id, $request->getContent()
            )
        );
    }
}
