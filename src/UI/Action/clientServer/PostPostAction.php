<?php

namespace App\UI\Action\clientServer;

use App\Domain\Model\Interfaces\PostInterface;
use App\Domain\Model\Post;
use App\Domain\Saver\Interfaces\PostSaverInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PostPostAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var PostSaverInterface
     */
    private $postSaver;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * PostPostAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param PostSaverInterface    $postSaver
     * @param SerializerInterface   $serializer
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        PostSaverInterface $postSaver,
        SerializerInterface $serializer
    ) {
        $this->apiResponder = $apiResponder;
        $this->postSaver = $postSaver;
        $this->serializer = $serializer;
    }

    /**
     * @param Request $request
     *
     * @Route(
     *     path="/articles",
     *     name="posts_post",
     *     methods={"POST"}
     * )
     *
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function postPost(Request $request): Response
    {
        /** @var PostInterface $post */
        return $this->apiResponder->response(
            $this->postSaver->save($request->getContent()),
            201
        );
    }
}
