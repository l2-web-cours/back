<?php

namespace App\UI\Action\clientServer;

use App\Domain\Loader\UsersLoader;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var UsersLoader
     */
    private $usersLoader;

    /**
     * UserAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param UsersLoader           $usersLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        UsersLoader $usersLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->usersLoader = $usersLoader;
    }

    /**
     * @return Response
     * @Route(
     *     path="/users",
     *     name="users"
     * )
     */
    public function getUsers(): Response
    {
        return $this->apiResponder->response($this->usersLoader->load());
    }
}
