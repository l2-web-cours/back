<?php

namespace App\UI\Action\clientServer;

use App\Domain\Loader\Interfaces\PostLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostGetAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var PostLoaderInterface
     */
    private $postLoader;

    /**
     * PostGetAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param PostLoaderInterface   $postLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        PostLoaderInterface $postLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->postLoader = $postLoader;
    }

    /**
     * @Route(
     *     path="/articles/{id}",
     *     requirements={"id"="\d+"},
     *     defaults={"id"=null},
     *     methods={"GET"}
     * )
     * @param int $id
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function getAction(?int $id = null): Response
    {
        $post = $this->postLoader->load($id);

        if ($id) {
            return $post->getItem()
                ? $this->apiResponder->response($post)
                : $this->apiResponder->response(null, 404);
        }

        return $this->apiResponder->response($post);
    }
}
