<?php

namespace App\UI\Action\ajaxServer;

use App\Domain\Loader\Interfaces\PictureLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PicturesGet
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var PictureLoaderInterface
     */
    private $pictureLoader;

    /**
     * PicturesGet constructor.
     *
     * @param ApiResponderInterface  $apiResponder
     * @param PictureLoaderInterface $pictureLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        PictureLoaderInterface $pictureLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->pictureLoader = $pictureLoader;
    }

    /**
     * @Route(
     *     path="/pictures/{id}",
     *     name="ajax_get",
     *     requirements={"\d+"},
     *     methods={"GET"}
     * )
     * @param int $id
     *
     * @return Response
     */
    public function get(int $id): Response
    {
        if ($id < 1) {
            return $this->apiResponder->response(null, Response::HTTP_BAD_REQUEST);
        }

        $pictures = $this->pictureLoader->load($id);

        if (is_null($pictures)) {
            return $this->apiResponder->response(null, Response::HTTP_NOT_FOUND);
        }

        return $this->apiResponder->response($pictures);
    }
}
