<?php

namespace App\UI\Responder;

use App\Application\Interfaces\OutInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ApiResponder implements ApiResponderInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * ApiResponder constructor.
     *
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * {@inheritDoc}
     */
    public function response(
        ?OutInterface $content = null,
        ?int $status = 200,
        ?array $headers = []
    ): Response {
        $headers['Content-Type'] = 'application/json';
        return new Response(
            $content ? $this->serializer->serialize($content, 'json') : null,
            $status,
            $headers
        );
    }
}
