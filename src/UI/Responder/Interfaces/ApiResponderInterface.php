<?php

namespace App\UI\Responder\Interfaces;

use App\Application\Interfaces\OutInterface;
use Symfony\Component\HttpFoundation\Response;

interface ApiResponderInterface
{
    /**
     * @param OutInterface|null $content
     * @param int|null          $status
     * @param array|null        $headers
     *
     * @return Response
     */
    public function response(?OutInterface $content = null, ?int $status = 200, ?array $headers = []): Response;
}